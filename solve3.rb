#require 'ruby2d'
require 'parallel'
require 'connected'

class Core
  attr_reader :node_store
  attr_reader :strands
  attr_reader :min_x, :min_y, :max_x, :max_y

  def initialize(input)
    @strands = []
    @node_store = {}
    input.each do |strand_input|
      strand = Strand.new(strand_input, self)
      @strands << strand
    end

    @min_x, @max_x = @strands.map{ |s| s.path }.flatten.minmax_by{ |n| n.x }.map { |n| n.x }
    @min_y, @max_y = @strands.map{ |s| s.path }.flatten.minmax_by{ |n| n.y }.map { |n| n.y }
  end

  def get_or_create_node(x, y = nil)
    key = "#{x},#{y}" unless y.nil?
    key = "#{x[0]},#{x[0]}" if y.nil?
    return @node_store[key] if @node_store.key?(key)
    node = Node.new(x, y)
    node.core = self
    @node_store[key] = node
    node
  end

  #def draw_start_area_box
  #  draw_area_box(:start, 'green')
  #end

  #def draw_finish_area_box
  #  draw_area_box(:finish, 'red')
  #end

  #def draw_core_box
  #  Rectangle.new(x: @min_x, y: @min_y, width: @max_x - @min_x, height: @max_y - @min_y, color: 'gray', z: 0)
  #end

  #private

  #def draw_area_box(area, color)
  #  nodes = @strands.filter{ |s| s.endpoint == area }.map{ |s| s.path.last }
  #  min_x, max_x = nodes.map{ |n| n.x }.minmax
  #  min_y, max_y = nodes.map{ |n| n.y }.minmax
  #  Rectangle.new(x: min_x, y: min_y, width: max_x - min_x, height: max_y - min_y, color: color, z: 10)
  #end

  def nodes_in_area(area)
    @strands.filter{ |s| s.endpoint == area }.map{ |s| s.path.last }
  end
end

class Link < Connected::GenericConnection
end

class Node
  include Connected::Vertex
  attr_reader :x, :y
  attr_accessor :area
  attr_reader :connections
  attr_reader :name

  def initialize(x, y = nil)
    @connections = []

    if x.is_a? Array
      @x = x[0].to_i
      @y = x[1].to_i
    else
      @x = x.to_i
      @y = y.to_i
    end
    @name = "#{@x},#{@y}"
  end

  def core=(core)
    @core = core
  end

  def connect_to(other)
    return if neighbors.include?(other)

    connections << Link.new(
      from: self, to: other, metric: 1, state: :open
    )

    other.connect_to(self)
  end

  def disconnect_from(other)
    connections.delete_if { |c| c.to == other }
    other.disconnect_from(self) if other.neighbors.include?(self)
  end

  def u
    @core.get_or_create_node(@x, @y - 1)
  end

  def d
    @core.get_or_create_node(@x, @y + 1)
  end

  def r
    @core.get_or_create_node(@x + 1, @y)
  end

  def l
    @core.get_or_create_node(@x - 1, @y)
  end

  def hash
    "#{@x}#{@y}".to_i
  end

  def to_s
    "#{@x},#{@y}"
  end

  def to_string
    to_s
  end
end

class Strand
  attr_reader :start
  attr_reader :end
  attr_accessor :endpoint
  attr_reader :path

  def initialize(description, core)
    @description = description
    @path = []
    @core = core
    process_description
  end

  def to_s
    "[#{@path.map{ |e| e.to_s }.join(' ')}]"
  end

  def process_description
    if @description.end_with? 'F'
      @endpoint = :finish
    elsif @description.end_with? 'X'
      @endpoint = :wall
    elsif @description.end_with? 'S'
      @endpoint = :start
    end

    starting_point, path = @description.split ' '
    path = "" if path.nil?
    @start = @core.get_or_create_node(starting_point.split(','))
    @path << @start
    node = @start
    path.split(',').each do |dir|
      if dir.match?(/[LRUD]/)
        next_node = node.send(dir.downcase)
        node.connect_to next_node
        @path << next_node
        node = next_node
      end
    end
    @end = @path.last
    @path.last.area = @endpoint unless @endpoint.nil?
  end
end

input = File.readlines("./input3.txt").map { |l| l.chomp }

@core = Core.new(input)

#@core.draw_core_box
#@core.draw_start_area_box
#@core.draw_finish_area_box

#Parallel.each(@core.strands) do |strand|
#  strand.draw_path
#end

#show

require 'pp'
pp @core.node_store.keys.size
