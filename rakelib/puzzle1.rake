require 'parallel'
require 'humanoid_hunt/puzzle1'

namespace 'puzzle1' do
  desc 'Solve puzzle 1'
  task :solve do
    puts 'Running puzzle 1 solver...'

    input = File.readlines('./data/input1.txt').map { |l| l.chomp }

    new_input = Parallel.map(input) do |one_line|
      HumanoidHunt::Puzzle1.build_number_array(one_line)
    end

    found_chars = Parallel.map(new_input) do |one_stream|
      HumanoidHunt::Puzzle1.follow_the_trail(one_stream)
    end

    puts "Solution: #{found_chars.join}"
  end
end
