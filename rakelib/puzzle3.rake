require 'parallel'
require 'humanoid_hunt/puzzle2'

namespace 'puzzle3' do
  desc 'Solve puzzle 3'
  task :solve do
    puts 'Running puzzle 3 solver...'

    input = File.readlines('./data/input3.txt').map { |l| l.chomp }
  end
end
