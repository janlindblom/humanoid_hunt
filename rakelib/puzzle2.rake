require 'parallel'
require 'humanoid_hunt/puzzle2'

namespace 'puzzle2' do
  desc 'Solve puzzle 2'
  task :solve do
    puts 'Running puzzle 2 solver...'

    input = File.readlines('./data/input2.txt').map { |l| l.chomp }.first
    secret_chars = []

    secret_chars << HumanoidHunt::Puzzle2.find_most_frequent_char(input)
    current_char = secret_chars.first

    while current_char != ";"
      current_char = HumanoidHunt::Puzzle2.find_most_frequent_follower(current_char, input)
      secret_chars << current_char unless current_char == ";"
    end

    puts "Solution: #{secret_chars.join}"
  end
end
