module HumanoidHunt
  class Puzzle2
    class << self
      def find_all_followers(input, char)
        followers = []
        current_index = 0
        while !current_index.nil?
          current_index = input.index(char, current_index)
          unless current_index.nil?
            followers << input[current_index..(current_index + 1)].chars.last
            current_index += 1
          end
        end
        followers
      end

      def build_a_frequency_map(input, char = nil)
        map = {}
        unless char
          input.chars.each do |char|
            map[char] = 1 unless map[char]
            map[char] += 1 if map[char]
          end
        else
          map = build_a_frequency_map(find_all_followers(input, char).join)
        end
        map
      end

      def find_most_frequent_char(input)
        map = build_a_frequency_map(input)

        occurs = map.sort_by(&:last)
        occurs.last.first
      end

      def find_most_frequent_follower(character,input)
        map = build_a_frequency_map(input, character)
        occurs = map.sort_by(&:last)
        occurs.last.first
      end
    end
  end
end
