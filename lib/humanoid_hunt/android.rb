require 'parallel'
require 'connected'
require 'humanoid_hunt/android/core'
require 'humanoid_hunt/android/strand_builder'
require 'humanoid_hunt/android/strand'
require 'humanoid_hunt/android/node'
require 'humanoid_hunt/android/link'

module HumanoidHunt
  class Android
    attr_reader :core

    def initialize(input)
      @core = HumanoidHunt::Android::Core.new(input)
    end
  end
end
