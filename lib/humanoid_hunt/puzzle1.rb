module HumanoidHunt
  class Puzzle1
    class << self
      def read_byte(byte_bits)
        byte_bits.to_i(2)
      end

      def get_first_byte(bit_string)
        return [bit_string[0..7], bit_string[8..-1]] if bit_string.length > 8
        [bit_string, ""]
      end

      def build_number_array(bit_string)
        number_array = []
        while bit_string.length > 0
          byte_string, bit_string = get_first_byte(bit_string)
          number_array << read_byte(byte_string)
        end
        number_array
      end

      def follow_the_trail(stream)
        index = 0
        finished = false
        we_just_started = true
        found_character = nil
        while !finished
          byte = stream[index]
          if byte < stream.length # byte is an index in the stream
            index = byte
            we_just_started = false if we_just_started
          else # byte is not an index in the stream
            if we_just_started # we haven't found the first index yet
              index += 1
            else
              finished = true
              found_character = byte.chr
            end
          end
        end
        return found_character
      end
    end
  end
end
