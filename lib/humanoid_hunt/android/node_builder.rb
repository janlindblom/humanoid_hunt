require 'humanoid_hunt/android/core'

module HumanoidHunt
  class Android
    class NodeBuilder
      def initialize(core)
        @core = core
      end

      def build_node(x, y)
        HumanoidHunt::Android::Node.new(x, y)
      end

      def u(node)

      end

      def d(node)

      end

      def l(node)

      end

      def r(node)

      end
    end
  end
end
