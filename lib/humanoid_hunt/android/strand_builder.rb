module HumanoidHunt
  class Android
    class StrandBuilder

      def initialize(core)
        @core = core
      end

      def build_strand(description)
        strand = Strand.new(description)
        process_description(strand)
        strand
      end

      private

      def process_description(strand)
        if strand.description.end_with? 'F'
          strand.endpoint = :finish
        elsif strand.description.end_with? 'X'
          strand.endpoint = :wall
        elsif strand.description.end_with? 'S'
          strand.endpoint = :start
        end

        starting_point, path = strand.description.split ' '
        path = '' if path.nil?
        strand.start = @core.get_or_create_node(starting_point.split(','))
        strand.path << strand.start
        node = strand.start
        node_builder = HumanoidHunt::Android::NodeBuilder.new(@core)
        path
          .split(',')
          .each do |dir|
            if dir.match?(/[LRUD]/)
              next_node = node.send(dir.downcase)
              node.connect_to next_node
              strand.path << next_node
              node = next_node
            end
          end
        strand.end = strand.path.last
        strand.path.last.area = strand.endpoint unless strand.endpoint.nil?
      end
    end
  end
end
