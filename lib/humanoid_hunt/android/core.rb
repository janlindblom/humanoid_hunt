require 'humanoid_hunt/android/strand_builder'
require 'humanoid_hunt/android/node'

module HumanoidHunt
  class Android
    class Core
      attr_reader :node_store
      attr_reader :strands
      attr_reader :min_x, :min_y, :max_x, :max_y

      def initialize(input)
        @strands = []
        @node_store = {}
        builder = HumanoidHunt::Android::StrandBuilder.new(self)
        input.each do |strand_input|
          @strands << builder.build_strand(strand_input)
        end

        @min_x, @max_x = @strands.map{ |s| s.path }.flatten.minmax_by{ |n| n.x }.map { |n| n.x }
        @min_y, @max_y = @strands.map{ |s| s.path }.flatten.minmax_by{ |n| n.y }.map { |n| n.y }
      end

      def get_or_create_node(x, y = nil)
        key = "#{x},#{y}" unless y.nil?
        key = "#{x[0]},#{x[0]}" if y.nil?
        return @node_store[key] if @node_store.key?(key)
        builder = HumanoidHunt::Android::NodeBuilder.new(self)
        node = builder.build_node(x, y)
        node.core = self
        @node_store[key] = node
        node
      end

      def nodes_in_area(area)
        @strands.filter{ |s| s.endpoint == area }.map{ |s| s.path.last }
      end
    end
  end
end
