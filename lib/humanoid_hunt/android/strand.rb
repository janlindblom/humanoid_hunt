require 'humanoid_hunt/android/core'

module HumanoidHunt
  class Android
    class StrandBuilder
      def initialize(core)
        @core = core
      end

      def build_strand(description)

      end
    end
    class Strand
      attr_reader :start
      attr_reader :end
      attr_accessor :endpoint
      attr_reader :path

      def initialize(description)
        @description = description
        @path = []
        #@core = core
        #process_description
      end

      def to_s
        "[#{@path.map { |e| e.to_s }.join(' ')}]"
      end

      def process_description
        if @description.end_with? 'F'
          @endpoint = :finish
        elsif @description.end_with? 'X'
          @endpoint = :wall
        elsif @description.end_with? 'S'
          @endpoint = :start
        end

        starting_point, path = @description.split ' '
        path = '' if path.nil?
        @start = @core.get_or_create_node(starting_point.split(','))
        @path << @start
        node = @start
        path
          .split(',')
          .each do |dir|
            if dir.match?(/[LRUD]/)
              next_node = node.send(dir.downcase)
              node.connect_to next_node
              @path << next_node
              node = next_node
            end
          end
        @end = @path.last
        @path.last.area = @endpoint unless @endpoint.nil?
      end
    end
  end
end
