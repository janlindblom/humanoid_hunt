require 'humanoid_hunt/version'
require 'humanoid_hunt/puzzle1'
require 'humanoid_hunt/puzzle2'
require 'humanoid_hunt/android'

module HumanoidHunt
  class Error < StandardError; end
  # Your code goes here...
end
